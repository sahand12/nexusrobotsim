## NEXUS Robot Simulation 

this simulation uses [nexus mecanum platform](https://github.com/RBinsonB/nexus_4wd_mecanum_simulator). The platform has a D435 Intel Realsense depth sensor and a gazebo world with multiple objects in it including a car wheel. Optimal for visual servoing use cases. 

![](Screenshot_20240216_122212.png)

